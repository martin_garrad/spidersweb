#include <iostream>
using namespace std;

const double K1_DEFAULT = 5;
const double K3_DEFAULT = 0;
const double D1_DEFAULT = .1;
const double D3_DEFAULT = 0;

const int SPIRAL = 0;
const int RADIAL = 1;


//Reminder: Caps for class name
class Spring
{

    private:

        int id;
        
        double resting_length;  // resting length
        double length;   // current length.
        double extension = 0; // difference between current length and resting length
        double old_extension = 0;
        double extension_derivative = 0; // derivative of extension
        double initial_length;

        double k1 = K1_DEFAULT;
        double k3 = K3_DEFAULT;
        double d1 = D1_DEFAULT;
        double d3 = D3_DEFAULT;

        // Two nodes that are connected by this spring
        int node_a;
        int node_b;

        //Total force applied by this spring
        double force_magnitude = 0;
        //Force components
        double force_x = 0;
        double force_y = 0;
        double force_z = 0;
        int debug_level = 0;

        //These represent the end points of the spring; store to make printing consistent with nodes
        double x1;
        double x2;
        double y1;
        double y2;
        double z1;
        double z2;

        int type;

    public:

        // Default constructor to load in spring and damping coefficeints
        // Todo: We should make this classe more general - the stiffness and damping functiosn should be overloaded
        // So we can implement variations of that
        Spring(int id, int node_a, int node_b, double resting_length, double initial_length); //Make a spring with default stiffness params
        Spring(int id, int node_a, int node_b, double resting_length, double initial_length, double k1, double d1); //Make a linear spring
        Spring(int id, int node_a, int node_b, double resting_length, double initial_length, double k1, double d1, double k3, double d3);

        //The equation to change the force due to the spring
        // Todo: Better name is needed, what does it do acutally? get_force? update_force, etc..

        void update2d(double node_a_x_position, double node_a_y_position, double node_b_x_position, double node_b_y_position, double dt);
        void update3d(double node_a_x_position, double node_a_y_position, double node_a_z_position, double node_b_x_position, double node_b_y_position,  double node_b_z_position, double dt);
        void print_state();
        void print_position();
        void print_force();
        void print_position_and_force();

        double calculate_force();
        double get_force_magnitude();
        double get_force_x();
        double get_force_y();
        double get_force_z();

        //Return x1 and x2
        double get_length();
        double get_extension();
        double get_extension_derivative();

        void reset();

        double get_initial_length();
        double get_resting_length();
        void set_resting_length(double new_resting_length);
        void set_to_resting_length();

        int get_node_a_id();
        int get_node_b_id();
        int get_id();

        double get_k1();
        double get_k3();
        double get_d1();
        double get_d3();

        void set_k1(double new_k1);
        void set_k3(double new_k3);
        void set_d1(double new_d1);
        void set_d3(double new_d3);

        void set_type(int type);
        int get_type();

        void set_debug_level(int new_level);
        void print_debug();



};