#include "Eigen/Dense"
using namespace Eigen;

class SpringMassNetwork
{
	private:
		int num_nodes;
		int num_input_nodes;
		int num_springs;
		vector<Node> nodes;
		vector<Spring> springs;

		double tmax;
		double dt;
		int num_timesteps;

	public:

		SpringMassNetwork(vector<Node> &nodes, vector<Spring> &springs, double dt, double tmax);

		void reset();
		void run();
		void print_output();
		void print_edges();
};