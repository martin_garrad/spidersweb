#include <iostream>
using namespace std;


class Node
{

	private:

		int id;
    	// mass is 1 by default
		double mass = 1;

		//Original positions
		double initial_x_position;
		double initial_y_position;
		double initial_z_position;

		// Cartesian coordinates
	  	double x_position;
	  	double y_position;
	  	double z_position;


    	// Velocity
    	double x_velocity = 0;
    	double y_velocity = 0;
    	double z_velocity = 0;

    	// Acceleration
    	double x_acceleration = 0;
    	double y_acceleration = 0;
    	double z_acceleration = 0;

		// The velocity of the nodes initially it is zero.
    	// int connections;  // TODO: Still needed
    	bool input_node = false;

		// Input force applied to the Node
    	double input_force_x = 0;
    	// Force in y direction
    	double input_force_y = 0;
    	double input_force_z = 0;

    	// Node is globally fixed or not
    	bool fixed_node = false;

		//A bool check to determine whether a node has been updated or not.
		bool update_check = 0;


  	public:

  		//Default constructor, with caretesian coordinates
  		Node(int id, double x_position, double y_position, double z_position, double mass);

  		// Function to convert Node to node that receives and an input force
		void set_input(bool state);

		void set_mass(double new_mass);

		double get_mass();

  		// Checks if node is input node
		bool is_input();

  		//Returns fixed node
		bool is_fixed();

  		//Save original position in x and y of nodes.
		void reset_positions();


    	// Make node globally fixed
		void set_fixed(bool state);

    	//Show current position
		void print_position();
		void print_positions_and_velocities();

    	//Show xposition
    	// Todo: Same here: get_x_Position()
		double get_x_position();

    	//Show yposition
    	// Todo: detto
		double get_y_position();

		double get_z_position();

		double get_x_velocity();
		double get_y_velocity();
		double get_z_velocity();

		double get_x_acceleration();
		double get_y_acceleration();
		double get_z_acceleration();

		//This is the function that incrementally changes the nodes position in the next timestep;
    	// maybe instead of "change" use "update"
		void apply_force(double Fx, double Fy);
		void apply_force(double Fx, double Fy, double Fz);

		void reset_forces();
		void reset();

		void update(double dt);

		void reset_state();

  		//At every timestep, a node should be changed only once. A node should not be changed at every tiem
		void change_update_check();

		//Check to see if the node has been modified or not
		bool return_update_check();

		//Check to see if the node has been modified or not
		void zero_update_check();
};
